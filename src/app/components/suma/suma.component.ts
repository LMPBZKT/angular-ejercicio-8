import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-suma',
  templateUrl: './suma.component.html',
  styleUrls: ['./suma.component.css']
})
export class SumaComponent implements OnInit {
  num = 0;
  mensaje: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  Mayor(): void {
    if (this.num == 50) {
      this.num = 50;
      this.mensaje = true;
    } else {
      this.num = this.num + 1;
      this.mensaje = false;
    }
  }

  Menor(): void {
    if (this.num == 0) {
      this.num = 0;
      this.mensaje = true;
    } else {
      this.num = this.num - 1;
      this.mensaje = false;
    }
  }
}
